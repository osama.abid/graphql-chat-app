# graphql-subscriptions-chat
Chat app based on Subscriptions and Sockets on GraphQL

# Deployment:
- Create .env file in /server
- Go to parent directory
- Run *docker-compose up --build -d*

# Application have React based Client side and Node/GraphQL based backend 
